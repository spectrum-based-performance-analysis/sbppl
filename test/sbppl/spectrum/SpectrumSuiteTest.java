package sbppl.spectrum;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class SpectrumSuiteTest {

	private HitSpectrumSuite hSuite;
	private CodeBlock cb1;
	private CodeBlock cb2;
	private HitSpectrum hs;

	@Before
	public void setup() {
		hSuite = new HitSpectrumSuite();
		cb1 = new CodeBlock("sbppl.spectrum.HitSpectrum", "inc", "(CodeBlock)V", 16);
		cb2 = new CodeBlock("sbppl.spectrum.HitSpectrum", "setCount", "(CodeBlock, I)V", 21);
		hs = new HitSpectrum("singleRun", "V1.0");
	}

	@Test
	public void HitSpectrumSuiteWithoutSpectra() throws Exception {
		SimilarityCoefficientResult ochiai = hSuite.calcOchiai();
		assertTrue(ochiai.isEmpty());
	}

	@Test
	public void HitSpectrumSuiteWithOneSpectrum_AllBlocksAreEquallyPropable() throws Exception {
		hs.setContainsPerformanceProblem(true);
		hs.inc(cb1);
		hs.inc(cb2);
		hSuite.addSpectrum(hs);
		SimilarityCoefficientResult ochiai = hSuite.calcOchiai();
		assertEquals(2, ochiai.size());
		assertEquals(1, ochiai.get(cb1).intValue());
	}

	@Test
	public void HitSpectrumSuiteWithTwoDisjointSpectra() throws Exception {
		HitSpectrum hs2 = new HitSpectrum("secondRun", "V1.0");
		hs.setContainsPerformanceProblem(true);
		hs.inc(cb1);
		hs2.inc(cb2);
		hSuite.addSpectrum(hs);
		hSuite.addSpectrum(hs2);
		SimilarityCoefficientResult ochiai = hSuite.calcOchiai();
		assertEquals(2, ochiai.size());
		assertEquals(1, ochiai.get(cb1).intValue());
		assertEquals(0, ochiai.get(cb2).intValue());
	}

	@Test
	public void HitSpectrumSuiteWithTwoPartlyJointSpectra() throws Exception {
		HitSpectrum hs2 = new HitSpectrum("secondRun", "V1.0");
		hs.setContainsPerformanceProblem(true);
		hs.inc(cb1);
		hs2.inc(cb1);
		hs2.inc(cb2);
		hSuite.addSpectrum(hs);
		hSuite.addSpectrum(hs2);
		SimilarityCoefficientResult ochiai = hSuite.calcOchiai();
		assertEquals(2, ochiai.size());
		assertEquals(1 / Math.sqrt(2), ochiai.get(cb1).doubleValue(), 0.01);
		assertEquals(0, ochiai.get(cb2).doubleValue(), 0.01);
	}
}
