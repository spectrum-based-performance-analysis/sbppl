package sbppl.spectrum;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class HitSpectrumTest {

	private AbstractSpectrum spec;
	private CodeBlock block;

	@Before
	public void setUp() throws Exception {
		spec = new HitSpectrum("TestMehtodName", "V1.0");
		block = new CodeBlock("sbppl.AbstractSpectrum.AbstractSpectrum", "inc", "(CodeBlock)void", 0);
	}

	@After
	public void tearDown() throws Exception {
		spec = null;
		block = null;
	}

	@Test
	public void incCodeBlockByOne() throws Exception {
		spec.inc(block);
		assertEquals(1, spec.getCountFor(block));
	}

	@Test
	public void incCodeBlockTwoTimes_givesOne() throws Exception {
		spec.inc(block);
		spec.inc(block);
		assertEquals(1, spec.getCountFor(block));
	}

	@Test
	public void getCountForNotMentionedCodeBlock_givesZero() throws Exception {
		assertEquals(0, spec.getCountFor(block));
	}

	@Test
	public void setCountToOne() throws Exception {
		spec.setCount(block, 1);
		assertEquals(1, spec.getCountFor(block));
	}

	@Test
	public void setCountToTwo_throwsException() throws Exception {
		try {
			spec.inc(block);
			spec.setCount(block, 2);
		} catch (IllegalArgumentException e) {
			assertNotNull(e);
		}
		assertEquals(1, spec.getCountFor(block));

	}

}
