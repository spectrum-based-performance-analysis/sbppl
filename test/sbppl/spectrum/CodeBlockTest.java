package sbppl.spectrum;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CodeBlockTest {

	@Test
	public void testEquals() {
		CodeBlock block1 = new CodeBlock("sbppl.spectrum.CodeBlock", "equals", "(Object)boolean", 0);
		CodeBlock block2 = new CodeBlock("sbppl.spectrum.CodeBlock", "equals", "(Object)boolean", 0);
		assertTrue(block1.equals(block2));
	}

	@Test
	public void testEqualsWithDifferentMehtods() throws Exception {
		CodeBlock block1 = new CodeBlock("sbppl.spectrum.CodeBlock", "equals", "(Object)boolean", 0);
		CodeBlock block2 = new CodeBlock("sbppl.spectrum.CodeBlock", "CodeBlock", "(String;String;String)CodeBlock", 0);
		assertFalse(block1.equals(block2));
	}

	@Test
	public void testEqualsWithDifferentClasses() throws Exception {
		CodeBlock block1 = new CodeBlock("sbppl.spectrum.CodeBlock", "equals", "(Object)boolean", 0);
		CodeBlock block2 = new CodeBlock("sbppl.spectrum.HitSpectrum", "equals", "(Object)boolean", 0);
		assertFalse(block1.equals(block2));
	}

	@Test
	public void testEqualsWithSamePartId() throws Exception {
		CodeBlock block1 = new CodeBlock("sbppl.spectrum.CodeBlock", "equals", "(Object)boolean", 2, 0);
		CodeBlock block2 = new CodeBlock("sbppl.spectrum.CodeBlock", "equals", "(Object)boolean", 2, 0);
		assertTrue(block1.equals(block2));
	}

	@Test
	public void testEqualsWithdifferentPartId() throws Exception {
		CodeBlock block1 = new CodeBlock("sbppl.spectrum.CodeBlock", "equals", "(Object)boolean", 1, 0);
		CodeBlock block2 = new CodeBlock("sbppl.spectrum.CodeBlock", "equals", "(Object)boolean", 2, 0);
		assertFalse(block1.equals(block2));
	}

	@Test
	public void testEqualsWithNull() throws Exception {
		CodeBlock block1 = new CodeBlock("sbppl.spectrum.CodeBlock", "equals", "(Object)boolean", 0);
		assertFalse(block1.equals(null));
	}

}
