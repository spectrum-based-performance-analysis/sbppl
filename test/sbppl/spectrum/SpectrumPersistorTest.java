package sbppl.spectrum;

import static org.junit.Assert.assertEquals;

import java.nio.file.FileSystems;
import java.nio.file.Files;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SpectrumPersistorTest {

	private static final String filePath = "serialisierung.xml";
	private SpectrumPersistor persistor;
	private AbstractSpectrum countSpec;
	private CodeBlock block;
	private AbstractSpectrum specFromFile;
	private HitSpectrum hitSpec;

	@Before
	public void setUp() throws Exception {
		countSpec = new CountSpectrum("TestMethodName", "V1.1");
		hitSpec = new HitSpectrum("TestMethodTwo", "V2.3");
		hitSpec.setContainsPerformanceProblem(true);
		persistor = new SpectrumPersistor();
		block = new CodeBlock("sbppl.AbstractSpectrum.AbstractSpectrum", "inc", "(CodeBlock)void", 5);
	}

	@After
	public void tearDown() throws Exception {
		persistor = null;
		countSpec = null;
		hitSpec = null;
		block = null;
		specFromFile = null;
		Files.deleteIfExists(FileSystems.getDefault().getPath(filePath));
	}

	@Test
	public void hitSpectrumWithoutCounters() throws Exception {
		persistor.save(hitSpec, filePath);
		specFromFile = persistor.load(filePath);
		assertEquals(hitSpec.runIdentifier, specFromFile.runIdentifier);
		assertEquals(hitSpec.containsPerformanceProblem(), specFromFile.containsPerformanceProblem());
		assertEquals(hitSpec.versionIdentifier, specFromFile.versionIdentifier);
	}

	@Test
	public void countSpectrumWithOneCounter() throws Exception {
		countSpec.setCount(block, 14);
		persistor.save(countSpec, filePath);
		specFromFile = persistor.load(filePath);
		assertEquals(14, specFromFile.getCountFor(block));
	}

	@Test
	public void hitSpectrumWithTwoCounters() throws Exception {
		CodeBlock block2 = new CodeBlock("sbppl.spectrum.SpectrumPersistor", "save", "(AbstractSpectrum,String)void", 3,
				4);
		hitSpec.inc(block);
		hitSpec.inc(block2);
		persistor.save(hitSpec, filePath);
		specFromFile = persistor.load(filePath);
		assertEquals(1, specFromFile.getCountFor(block));
		assertEquals(1, specFromFile.getCountFor(block2));
	}

}
