package sbppl.spectrum;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CountSpectrumTest {

	private AbstractSpectrum spec;
	private CodeBlock block;

	@Before
	public void setUp() throws Exception {
		spec = new CountSpectrum("TestMethodName", "V1.1");
		block = new CodeBlock("sbppl.AbstractSpectrum.AbstractSpectrum", "inc", "(CodeBlock)void", 0);
	}

	@After
	public void tearDown() throws Exception {
		spec = null;
		block = null;
	}

	@Test
	public void incCodeBlockTwoTimes_givesTwo() {
		spec.inc(block);
		spec.inc(block);
		assertEquals(2, spec.getCountFor(block));
	}

	@Test
	public void setCodeBlockCountTo15() {
		spec.setCount(block, 15);
		assertEquals(15, spec.getCountFor(block));
	}

	@Test
	public void noNegativeCountsAllowed() throws Exception {
		try {
			spec.setCount(block, -3);
		} catch (IllegalArgumentException e) {
			assertNotNull(e);
		}
		assertEquals(0, spec.getCountFor(block));
	}

	@Test
	public void getAllCodeBlocks_emptySet() throws Exception {
		Set<CodeBlock> allCodeBlocks = spec.getAllCodeBlocks();
		assertNotNull(allCodeBlocks);
		assertEquals(0, allCodeBlocks.size());
	}

	@Test
	public void getAllCodeBlocks_returnsTheBlocksGiven() throws Exception {
		CodeBlock block2 = new CodeBlock("sbppl.spectrum.CountSpectrum", "getAllCodeBlocks", "()Set<CodeBlock>", 2);
		spec.setCount(block, 2);
		spec.inc(block2);
		Set<CodeBlock> allCodeBlocks = spec.getAllCodeBlocks();
		assertTrue(allCodeBlocks.contains(block));
		assertTrue(allCodeBlocks.contains(block2));
		assertEquals(2, allCodeBlocks.size());
	}

}
