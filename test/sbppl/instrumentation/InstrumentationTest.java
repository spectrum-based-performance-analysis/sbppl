package sbppl.instrumentation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.URL;
import java.net.URLClassLoader;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javassist.ClassPool;
import javassist.CtClass;
import sbppl.spectrum.CodeBlock;
import sbppl.spectrum.SpectrumType;

public class InstrumentationTest {

	public static interface SimpleInterface {

		int onlyNoneSense();

		double methodWithIf(int in);

		void secondMethod(String param);

		int methodWithBranch(int input);

	}

	public static class Simple implements SimpleInterface {

		/*
		 * (non-Javadoc)
		 * 
		 * @see sbppl.instrumentation.SimpleInterface#onlyNoneSense()
		 */
		@Override
		public int onlyNoneSense() {
			int a = 3 + 4;
			int b = a * 7;
			a = b;
			return a;
		}

		@Override
		public void secondMethod(String param) {
			String something = param + "abvde";
			something.toLowerCase();

		}

		@Override
		public int methodWithBranch(int input) {
			int output;
			switch (input) { // Branch 1
			case 1: { // Branch 2
				output = 1;
				break;
			}
			case 2: { // Branch 3
				output = 2;
				break;
			}
			default: { // Branch 4
				output = 6;
				break;
			}
			}
			return output; // Branch 5
		}

		public double methodWithIf(int in) {
			double out;
			if (in > 4) { // Branch 1
				out = in * 2;
				out += 4;
				out *= 2;
			} else {
				out = in / 2; // Branch 2
				out *= 1;
				out += 4;
			}
			return out; // Branch 3
		}

	}

	private CtClass simpleClass;
	private Instrumentor instr;
	private SimpleInterface simpleObj;

	@Before
	public void setUp() throws Exception {
		ClassPool pool = new ClassPool(true);
		simpleClass = pool.getCtClass(InstrumentationTest.class.getName() + "$Simple");
		// simpleClass.defrost();
		instr = new Instrumentor(SpectrumType.CountSpectrum, "SomeMethod", "V2.0");
		instr.instrument(simpleClass);
		simpleObj = (SimpleInterface) simpleClass
				.toClass(new URLClassLoader(new URL[0], getClass().getClassLoader()), Class.class.getProtectionDomain())
				.newInstance();
	}

	@After
	public void tearDown() throws Exception {
		simpleClass = null;
	}

	@Test
	public void getCodeBlockbyCtMethod() throws Exception {
		assertEquals(new CodeBlock("sbppl.instrumentation.InstrumentationTest$Simple", "onlyNoneSense", "()I", 0),
				instr.codeBlockByMethod(simpleClass.getMethod("onlyNoneSense", "()I")));
	}

	@Test
	public void insertCallToSpectrumIntoMethod() throws Exception {
		CodeBlock noneSenseBlock = instr.codeBlockByMethod(simpleClass.getDeclaredMethod("onlyNoneSense"));
		simpleObj.onlyNoneSense();
		assertEquals(1, Instrumentor.spec.getCountFor(noneSenseBlock));
	}

	@Test
	public void allMethodsOfClassShoudBeInstrumented() throws Exception {
		CodeBlock equalsBlock = instr.codeBlockByMethod(simpleClass.getMethod("secondMethod", "(Ljava/lang/String;)V"));
		simpleObj.secondMethod("Parameter");
		assertTrue(Instrumentor.spec.getAllCodeBlocks().contains(equalsBlock));

	}

	@Test
	public void branchesAreRecognizedCorrectly() throws Exception {
		simpleObj.methodWithBranch(2);
		assertTrue(Instrumentor.spec.getAllCodeBlocks().contains(
				new CodeBlock("sbppl.instrumentation.InstrumentationTest$Simple", "methodWithBranch", "(I)I", 3, 65)));
		assertFalse(Instrumentor.spec.getAllCodeBlocks().contains(
				new CodeBlock("sbppl.instrumentation.InstrumentationTest$Simple", "methodWithBranch", "(I)I", 4, 69)));
	}

	@Test
	public void branchTestWithIf() throws Exception {
		simpleObj.methodWithIf(2);
		assertTrue(Instrumentor.spec.getAllCodeBlocks().contains(
				new CodeBlock("sbppl.instrumentation.InstrumentationTest$Simple", "methodWithIf", "(I)D", 1, 78)));
		assertFalse(Instrumentor.spec.getAllCodeBlocks().contains(
				new CodeBlock("sbppl.instrumentation.InstrumentationTest$Simple", "methodWithIf", "(I)D", 2, 83)));
	}

	@Test
	public void testCorrectSrcLineInMethodWithBranch() throws Exception {
		simpleObj.methodWithBranch(4);
		CodeBlock branch4 = new CodeBlock("sbppl.instrumentation.InstrumentationTest$Simple", "methodWithBranch",
				"(I)I", 4, 66);
		for (CodeBlock codeBlock : Instrumentor.spec.getAllCodeBlocks()) {
			if (codeBlock.equals(branch4)) {
				assertTrue(codeBlock.srcLine == 69);
				return;
			}
		}
		fail("Branch 4 was not in spectrum");
	}
}
