package sbppl.instrumentation;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import sbppl.spectrum.CountSpectrum;
import sbppl.spectrum.HitSpectrum;
import sbppl.spectrum.SpectrumType;

public class InstrumentorTest {

	@Test
	public void instantiationTestHitSpectrum() {
		new Instrumentor(SpectrumType.HitSpectrum, "TestRun", "V1.1");
		assertEquals(Instrumentor.spec.getClass(), HitSpectrum.class);
	}

	@Test
	public void instantiationTestCountSpectrum() {
		new Instrumentor(SpectrumType.CountSpectrum, "TestRun", "V1.1");
		assertEquals(Instrumentor.spec.getClass(), CountSpectrum.class);
	}

}
