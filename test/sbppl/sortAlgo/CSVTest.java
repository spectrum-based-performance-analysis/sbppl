package sbppl.sortAlgo;

import static org.junit.Assert.*;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CSVTest {

	private CSV csv;
	private static String sourceFile, targetFile;
	private ArrayList<Integer> readData;
	private static ArrayList<Integer> expectedData;
	private static Map<String, ArrayList<Integer>> outputData;

	@Rule
	public final ExpectedException exception = ExpectedException.none();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		sourceFile = "TestCSVRead.csv";
		targetFile = "TestCSVWrite.csv";
		File file = new File("DataSets/" + targetFile);
		if (!file.exists()) {
			// delete file before
		}
		expectedData = new ArrayList<Integer>() {
			private static final long serialVersionUID = 1L;

			{
				add(1);
				add(2);
				add(3);
				add(4);
				add(5);
			}
		};
		outputData = new TreeMap<String, ArrayList<Integer>>();
		outputData.put("ExpectedData", expectedData);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		sourceFile = null;
		targetFile = null;
		expectedData = null;
		outputData = null;
	}

	@Before
	public void setUp() throws Exception {
		csv = new CSV(sourceFile);

	}

	@After
	public void tearDown() throws Exception {
		csv = null;
	}

	@Test
	public void testCSV() {
		sourceFile = "SourceFile";
		csv = new CSV(sourceFile);
		assertEquals(sourceFile, csv.getSourcePath());
	}

	@Test
	public void testReadData_fileExists() {
		sourceFile = "TestCSVRead.csv";
		csv = new CSV(sourceFile);
		readData = csv.readData();
		assertEquals(expectedData, readData);
	}

	@Test
	public void testReadData_fileNotExists() {
		sourceFile = "NotExisting.csv";
		csv = new CSV(sourceFile);
		readData = csv.readData();
		assertNull(readData);
	}

	@Test
	public void testWriteData() {
		// this test is stupid, need to ask
		try {
			csv.writeData(outputData, "TestCSVWrite.csv");
			assertTrue(true);
		} catch (Exception e) {
			assertFalse(false);
		}
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testGetIntegerList_convertPossible() {

		ArrayList<String> stringList = new ArrayList<String>() {
			private static final long serialVersionUID = 1L;

			{
				add("1");
				add("2");
				add("3");
				add("4");
				add("5");
			}
		};
		ArrayList<Integer> intList = new ArrayList<Integer>() {
			private static final long serialVersionUID = 1L;

			{
				add(1);
				add(2);
				add(3);
				add(4);
				add(5);
			}
		};
		@SuppressWarnings("rawtypes")
		Class[] cArg = new Class[1];
		cArg[0] = ArrayList.class;
		ArrayList<Integer> convertedList = null;
		Method method;
		try {
			method = CSV.class.getDeclaredMethod("getIntegerList", cArg);
			method.setAccessible(true);
			Object[] argObjects = new Object[1];
			argObjects[0] = stringList;
			convertedList = (ArrayList<Integer>) method.invoke(csv, argObjects);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertEquals(intList, convertedList);
	}

	@Test
	public void testGetIntegerList_convertNotPossible() throws Throwable {
		exception.expect(NumberFormatException.class);
		ArrayList<String> stringList = new ArrayList<String>() {
			private static final long serialVersionUID = 1L;

			{
				add("A");
				add("B");
				add("C");
				add("D");
				add("E");
			}
		};

		@SuppressWarnings("rawtypes")
		Class[] cArg = new Class[1];
		cArg[0] = ArrayList.class;
		Method method;
		method = CSV.class.getDeclaredMethod("getIntegerList", cArg);
		method.setAccessible(true);
		Object[] argObjects = new Object[1];
		argObjects[0] = stringList;
		try{
		method.invoke(csv, argObjects);
		}catch (InvocationTargetException e){
			throw e.getTargetException();
		}


	}

	@SuppressWarnings("unchecked")
	@Test
	public void testGetStringList_convertPossible() {
		ArrayList<String> stringList = new ArrayList<String>() {
			private static final long serialVersionUID = 1L;

			{
				add("1");
				add("2");
				add("3");
				add("4");
				add("5");
			}
		};
		ArrayList<Integer> intList = new ArrayList<Integer>() {
			private static final long serialVersionUID = 1L;

			{
				add(1);
				add(2);
				add(3);
				add(4);
				add(5);
			}
		};
		@SuppressWarnings("rawtypes")
		Class[] cArg = new Class[1];
		cArg[0] = ArrayList.class;
		ArrayList<String> convertedList = null;
		Method method;
		try {
			method = CSV.class.getDeclaredMethod("getStringList", cArg);
			method.setAccessible(true);
			Object[] argObjects = new Object[1];
			argObjects[0] = intList;
			convertedList = (ArrayList<String>) method.invoke(csv, argObjects);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertEquals(stringList, convertedList);
	}

}
