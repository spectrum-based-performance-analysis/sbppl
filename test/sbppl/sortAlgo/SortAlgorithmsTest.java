package sbppl.sortAlgo;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class SortAlgorithmsTest {

	private ArrayList<Integer> unsortedData, expectedData;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		unsortedData = new ArrayList<Integer>() {
			private static final long serialVersionUID = 1L;
			{
				add(2);
				add(4);
				add(8);
				add(9);
				add(6);
				add(1);
				add(5);
				add(3);
				add(7);
			}
		};
		expectedData = new ArrayList<Integer>() {
			private static final long serialVersionUID = 1L;
			{
				add(1);
				add(2);
				add(3);
				add(4);
				add(5);
				add(6);
				add(7);
				add(8);
				add(9);
			}
		};
	}

	@After
	public void tearDown() throws Exception {
		unsortedData = null;
		expectedData = null;
	}

	@Test
	public void testSelectionSort() {
		ArrayList<Integer> sortedData = SortAlgorithms.selectionSort(unsortedData, unsortedData.size() - 1);
		assertEquals(sortedData, expectedData);
	}

	@Test
	public void testInsertionSort() {
		ArrayList<Integer> sortedData = SortAlgorithms.insertionSort(unsortedData, unsortedData.size() - 1);
		assertEquals(sortedData, expectedData);
	}

	@Test
	public void testMergeSort() {
		ArrayList<Integer> sortedData = SortAlgorithms.mergeSort(unsortedData, unsortedData.size() - 1);
		assertEquals(sortedData, expectedData);
	}

	@Test
	public void testMerge() {
		ArrayList<Integer> leftSideData = new ArrayList<Integer>() {
			private static final long serialVersionUID = 1L;
			{
				add(2);
				add(4);
				add(6);
				add(8);
				add(9);
			}
			
		
			
		};
		ArrayList<Integer> rightSideData = new ArrayList<Integer>() {
			private static final long serialVersionUID = 1L;
			{
				add(1);
				add(3);
				add(5);
				add(7);
			}
		};
		
		
		ArrayList<Integer> sortedData = SortAlgorithms.merge(leftSideData, rightSideData);
		assertEquals(sortedData, expectedData);
	}

}
