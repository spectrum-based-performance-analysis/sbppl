# Spectrum Based Performance Analysis

## Build
The ant task "export" runs all unit tests and on success exports the sbppl.jar to bin

``` shell
ant export
```

## Usage for instrumentation
To obtain a spectrum the sbppl.jar has to be used as javaagent **and**
be in the classpath of the execution. The easiest way to achieve this
is to place the `sbppl.jar` into the same directory as the application. 

``` shell
java -javaagent:sbppl.jar -jar application_to_analyze.jar
```

To configure the behavior of the instrumentation a config file with
the name `sbppl.conf` can be placed into the directory of execution. If
no config file is found the following contents are assumed:

``` apacheconf
version=unknown version
runIdentifier=Sun Oct 09 12\:20\:57 CEST 2016
spectrumType=CountSpectrum
includes=
excludes=sun;java;sbppl.instrumentation;sbppl.spectrum
savePath=results
```

Excludes and includes match against the start of the fully qualified
class name. Excludes are checked before includes are checked. If the
values are blank everything is matched.

The spectra is then saved to
`savePath/runIdentifier_version_timestamp.xml` and the sbppl.conf is 
saved as saved as `savePath/runIdentifier_version_timestamp_sbppl.conf`
for future reference.

## Usage for analyzing spectra

The `spbbl.jar` can also be used for analyzing saved spectra. It takes
the used spectrum type and the xml files of the spectra as parameters.

``` shell
java -jar sbppl.jar HitSpectrum results/*.xml
```

It prints the results to the standard output in csv format, so that
they can be piped into a file for further analysis.

``` shell
java -jar sbppl.jar HitSpectrum results/*.xml > test.csv
```

