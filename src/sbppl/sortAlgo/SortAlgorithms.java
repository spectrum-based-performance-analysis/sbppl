package sbppl.sortAlgo;

import java.util.ArrayList;

public class SortAlgorithms {

	public static ArrayList<Integer> selectionSort(ArrayList<Integer> unsortedData, int elements) {
		int index, index_small, value, value_small;
		if (unsortedData.size() <= 1) {
			return unsortedData;
		}
		for (index = 0; index < elements; index++) {
			/* current position */
			value = index;

			for (index_small = index + 1; index_small <= elements; index_small++) {
				// is there a smaller element
				if (unsortedData.get(index_small) < unsortedData.get(value)) {
					value = index_small;
				}
			}
			// put smallest element at current position
			if (value != index) {
				value_small = unsortedData.get(value);
				unsortedData.set(value, unsortedData.get(index));
				unsortedData.set(index, value_small);
			}
		}
		return unsortedData;
	}

	public static ArrayList<Integer> insertionSort(ArrayList<Integer> unsortedData, int elements) {
		int index, sortIn_value, compare_index;
		if (unsortedData.size() <= 1) {
			return unsortedData;
		}
		for (index = 0; index <= elements; index++) {
			sortIn_value = unsortedData.get(index);
			compare_index = index;
			while (compare_index >= 1 && unsortedData.get(compare_index - 1) > sortIn_value) {
				unsortedData.set(compare_index, unsortedData.get(compare_index - 1));
				compare_index -= 1;
			}
			unsortedData.set(compare_index, sortIn_value);
		}
		return unsortedData;
	}

	public static ArrayList<Integer> mergeSort(ArrayList<Integer> unsortedData, int elements) {
		ArrayList<Integer> leftSide, rightSide;
		if (unsortedData.size() <= 1) {
			return unsortedData;
		}
		leftSide = new ArrayList<Integer>();
		rightSide = new ArrayList<Integer>();

		for (int i = 0; i < unsortedData.size() / 2; i++) {
			leftSide.add(unsortedData.get(i));
		}
		leftSide = mergeSort(leftSide, leftSide.size());
		for (int j = unsortedData.size() / 2; j < unsortedData.size(); j++) {
			rightSide.add(unsortedData.get(j));
		}
		rightSide = mergeSort(rightSide, rightSide.size());

		return merge(leftSide, rightSide);

	}

	public static ArrayList<Integer> merge(ArrayList<Integer> leftSide, ArrayList<Integer> rightSide) {
		ArrayList<Integer> sortedData = new ArrayList<Integer>();
		while (!leftSide.isEmpty() && !rightSide.isEmpty()) {
			if (leftSide.get(0) <= rightSide.get(0)) {
				sortedData.add(leftSide.get(0));
				leftSide.remove(0);
			} else {
				sortedData.add(rightSide.get(0));
				rightSide.remove(0);
			}
		}
		while (!leftSide.isEmpty()) {
			sortedData.add(leftSide.get(0));
			leftSide.remove(0);
		}
		while (!rightSide.isEmpty()) {
			sortedData.add(rightSide.get(0));
			rightSide.remove(0);
		}

		return sortedData;
	}

	public static ArrayList<Integer> bubbleSort(ArrayList<Integer> unsortedData) {
		int elements = unsortedData.size();
		boolean swapped = true;
		while (swapped) {
			swapped = false;
			for (int j = 1; j < elements; j++) {
				int elementBefore = unsortedData.get(j - 1);
				int elementAfter = unsortedData.get(j);
				if (elementBefore > elementAfter) {
					unsortedData.set(j - 1, elementAfter);
					unsortedData.set(j, elementBefore);
					swapped = true;
				}

			}

		}
		return unsortedData;
	}
}
