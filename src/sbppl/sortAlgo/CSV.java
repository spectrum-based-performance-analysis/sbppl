package sbppl.sortAlgo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public class CSV {

	private String sourcePath;

	public CSV(String sourcePath) {
		this.sourcePath = sourcePath;
	}

	public String getSourcePath() {
		return sourcePath;
	}

	public ArrayList<Integer> readData() {
		BufferedReader reader = null;
		String line = "";
		ArrayList<String> splitRead = new ArrayList<String>();
		String cvsSplitBy = ";";
		try {
			File file = new File("DataSets/" + sourcePath);
			// System.out.println(file.getPath());
			reader = new BufferedReader(new FileReader(file));
			while ((line = reader.readLine()) != null) {
				splitRead.addAll(Arrays.asList(line.split(cvsSplitBy)));
			}

		} catch (FileNotFoundException fnfe) {
			// TODO Auto-generated catch block
			// fnfe.printStackTrace();
			return null;
		} catch (IOException ioe) {
			// TODO Auto-generated catch block
			ioe.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		if (splitRead.get(0) instanceof String && !splitRead.get(0).matches("\\d+")) {
			splitRead.remove(0);
		}

		return getIntegerList(splitRead);
	}

	public void writeData(Map<String, ArrayList<Integer>> outputData, String filename) {
		File file = new File("DataSets/" + filename);
		FileWriter fw = null;
		BufferedWriter writer = null;
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			fw = new FileWriter(file);
			writer = new BufferedWriter(fw);
			for (String key : outputData.keySet()) {
				ArrayList<String> writeList = getStringList(outputData.get(key));
				String listString = key + ";";
				for (String s : writeList) {
					listString += s + ";";
				}
				if (listString.charAt(listString.length() - 1) == (';')) {
					listString = listString.substring(0, listString.length() - 1);
				}
				writer.write(listString);
				writer.write("\r\n");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				writer.close();
				fw.close();
				// System.out.println("csv printed to " + file);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private ArrayList<Integer> getIntegerList(ArrayList<String> stringList) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		for (String stringValue : stringList) {
			// Convert String to Integer, and store it into integer array
			// list.
			result.add(Integer.parseInt(stringValue));
		}
		return result;
	}

	private ArrayList<String> getStringList(ArrayList<Integer> intList) {
		ArrayList<String> result = new ArrayList<String>();
		for (Integer intValue : intList) {
			result.add(intValue.toString());
		}
		return result;
	}
}
