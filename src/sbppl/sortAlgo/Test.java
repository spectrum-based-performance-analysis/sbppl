package sbppl.sortAlgo;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class Test {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		String sourcePath = args[0];
		CSV sourceData = new CSV(sourcePath);
		ArrayList<Integer> unsortedData = sourceData.readData();
		ArrayList<Integer> selectionSort = SortAlgorithms.selectionSort((ArrayList<Integer>) unsortedData.clone(),
				unsortedData.size() - 1);
		ArrayList<Integer> insertionSort = SortAlgorithms.insertionSort((ArrayList<Integer>) unsortedData.clone(),
				unsortedData.size() - 1);
		ArrayList<Integer> mergeSort = SortAlgorithms.mergeSort((ArrayList<Integer>) unsortedData.clone(),
				unsortedData.size() - 1);
		ArrayList<Integer> bubbleSort = SortAlgorithms.bubbleSort((ArrayList<Integer>) unsortedData.clone());

		Map<String, ArrayList<Integer>> outputData = new TreeMap<String, ArrayList<Integer>>();
		outputData.put("Data", sourceData.readData());
		outputData.put("SelectionSort", selectionSort);
		outputData.put("InsertionSort", insertionSort);
		outputData.put("MergeSort", mergeSort);
		outputData.put("BubbleSort", bubbleSort);
		sourceData.writeData(outputData, "Result.csv");
	}

	// print a list
	public static void printList(ArrayList<Integer> list) {
		for (Integer print : list) {
			System.out.println(print.toString());
		}
	}
}
