package sbppl.spectrum;

/**
 * A spectrum that tracks how often each code block got executed
 * 
 * @author Florian Barth
 *
 */
public class CountSpectrum extends AbstractSpectrum {

	public CountSpectrum(String runIdentifier, String versionIdentifier) {
		super(runIdentifier, versionIdentifier);
	}

	@Override
	public void inc(CodeBlock block) {
		int count = getCountFor(block);
		setCount(block, count + 1);
	}

}
