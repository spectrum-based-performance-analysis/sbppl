package sbppl.spectrum;

/**
 * A Spectrum which only tracks which codeblocks got executed.
 * 
 * @author Florian Barth
 *
 */
public class HitSpectrum extends AbstractSpectrum {

	public HitSpectrum(String runIdentifier, String versionIdentifier) {
		super(runIdentifier, versionIdentifier);
	}

	@Override
	public void inc(CodeBlock block) {
		super.setCount(block, 1);
	}

	@Override
	public void setCount(CodeBlock block, int count) throws IllegalArgumentException {
		if (count > 1)
			throw new IllegalArgumentException("Hit spectra have maximum a count value of 1");
		super.setCount(block, count);
	}

}
