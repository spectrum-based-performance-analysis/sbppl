package sbppl.spectrum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A spectrum suite containing count spectra only
 * 
 * USAGE: Step 1: Fill spectrum suite with either a list of spectra (by
 * constructor) or by adding single spectra one by one (by addSpectrum()) Step
 * 2: Call the various calc...() methods to get a Map of all the calculated
 * values for a specific similarity coefficient (e.g. Cosine Similarity)
 * 
 * EXAMPLE 1: List<AbstractSpectrum> spectrumList = new
 * ArrayList<AbstractSpectrum>(); spectrumList.add(countSpectrum1);
 * spectrumList.add(countSpectrum2); spectrumList.add(countSpectrum3);
 * CountSpectrumSuite specSuite = new CountSpectrumSuite(spectrumList);
 * Map<CodeBlock, Double> cosineSimilarity = specSuite.calcCosineSimilarity();
 * 
 * EXAMPLE 2: CountSpectrumSuite specSuite = new CountSpectrumSuite();
 * specSuite.addSpectrum(countSpectrum1); specSuite.addSpectrum(countSpectrum2);
 * specSuite.addSpectrum(countSpectrum3); Map<CodeBlock, Double>
 * cosineSimilarity = specSuite.calcCosineSimilarity();
 * 
 * NOTE: Spectra lists with 0 elements and spectra lists containing spectra that
 * correspond to different CodeBlocks cause errors and should be avoided
 * 
 * @author Marc Schubert
 *
 */
public class CountSpectrumSuite extends AbstractSpectrumSuite {

	/**
	 * Store all the risk values for the various similarity coefficients
	 * 
	 * Every CodeBlock has a specific risk value for each similarity coefficient
	 */
	private SimilarityCoefficientResult cosineSimilarity, jaccard;

	/**
	 * Initiates spectrum suite with empty spectrum list
	 * 
	 * After calling this constructor the spectrum list has to be filled with
	 * spectra by calling addSpectrum()
	 */
	public CountSpectrumSuite() {
		this(new ArrayList<AbstractSpectrum>());
	}

	/**
	 * Initiates spectrum suite with pre-filled spectrum list
	 * 
	 * @param spectrumList
	 *            List of spectra, should already contain several spectra
	 */
	public CountSpectrumSuite(List<AbstractSpectrum> spectrumList) {
		super(spectrumList);

		cosineSimilarity = new SimilarityCoefficientResult();
		jaccard = new SimilarityCoefficientResult();
	}

	/**
	 * Calculates and returns all the Cosine Similarity risk values for the
	 * current spectrum list
	 * 
	 * The Cosine Similarity expresses the angle between the error vector and
	 * the individual code block vectors
	 * 
	 * @return a Map of the calculated risk values for this similarity
	 *         coefficient
	 */
	public SimilarityCoefficientResult calcCosineSimilarity() {

		long sum1, sum2, sum3;
		for (CodeBlock codeBlock : allCodeBlocks) {
			sum1 = 0;
			sum2 = 0;
			sum3 = 0;
			for (AbstractSpectrum spectrum : spectrumList) {
				sum2 += Math.pow(spectrum.getCountFor(codeBlock), 2);
				if (spectrum.containsPerformanceProblem()) {
					sum1 += spectrum.getCountFor(codeBlock);
					sum3++;
				}
			}
			cosineSimilarity.put(codeBlock, sum1 / (Math.sqrt(sum2) * Math.sqrt(sum3)));
		}

		return cosineSimilarity;
	}

	/**
	 * Calculates and returns all the Jaccard risk values for the current
	 * spectrum list
	 * 
	 * The original term looks like this: J(x,y) = sum[i](min(x[i],y[i])) /
	 * sum[i](max(x[i],y[i])) This term is meant to be for vectors containing
	 * arbitrary natural numbers but here one of these vectors is binary.
	 * Therefore the binary vector (error vector) is upscaled so that a 1 in the
	 * vector is replaced with the max-value of the individual code block
	 * vector: E.g. x=(1,4,5,3), y=(1,0,1,0) => max(x)=5 => y=(5,0,5,0)
	 * 
	 * @return a Map of the calculated risk values for this similarity
	 *         coefficient
	 */
	public SimilarityCoefficientResult calcJaccard() {

		// calculate the max-values of the code block vectors to upscale the
		// error vector
		Map<CodeBlock, Integer> maxCount = new HashMap<CodeBlock, Integer>();
		for (CodeBlock codeBlock : allCodeBlocks) {
			maxCount.put(codeBlock, 0);
			for (AbstractSpectrum spectrum : spectrumList) {
				if (spectrum.getCountFor(codeBlock) > maxCount.get(codeBlock))
					maxCount.put(codeBlock, spectrum.getCountFor(codeBlock));
			}
		}

		// calculate the actual risk values
		double sumMax, sumMin;
		for (CodeBlock codeBlock : allCodeBlocks) {
			sumMax = 0;
			sumMin = 0;
			for (AbstractSpectrum spectrum : spectrumList) {
				if (spectrum.containsPerformanceProblem()) {
					sumMax += maxCount.get(codeBlock);
					sumMin += spectrum.getCountFor(codeBlock);
				} else
					sumMax += spectrum.getCountFor(codeBlock);
			}
			jaccard.put(codeBlock, sumMin / sumMax);
		}

		return jaccard;
	}

}
