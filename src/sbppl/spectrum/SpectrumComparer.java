package sbppl.spectrum;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.xml.stream.XMLStreamException;

public class SpectrumComparer {

	public static void main(String[] args) throws FileNotFoundException, XMLStreamException {
		SimilarityCoefficientResult binary;
		SimilarityCoefficientResult jaccard;
		SimilarityCoefficientResult ochiai;
		SimilarityCoefficientResult russelAndRao;
		SimilarityCoefficientResult cosine;
		SpectrumType specType = SpectrumType.valueOf(args[0]);
		SpectrumPersistor persistor = new SpectrumPersistor();
		switch (specType) {
		case HitSpectrum:
			HitSpectrumSuite hitSuite = new HitSpectrumSuite();
			for (int i = 1; i < args.length; i++) {
				hitSuite.addSpectrum(persistor.load(args[i]));
			}
			binary = hitSuite.calcBinary();
			jaccard = hitSuite.calcJaccard();
			ochiai = hitSuite.calcOchiai();
			russelAndRao = hitSuite.calcRusselAndRao();
			List<HitCodeBlockAnalysisResult> hitList = new ArrayList<>(hitSuite.allCodeBlocks.size());
			for (CodeBlock cb : hitSuite.allCodeBlocks) {
				HitCodeBlockAnalysisResult codeBlockAnalysisResult = new HitCodeBlockAnalysisResult(cb, binary.get(cb),
						jaccard.get(cb), ochiai.get(cb), russelAndRao.get(cb));
				hitList.add(codeBlockAnalysisResult);
			}

			Collections.sort(hitList, new Comparator<HitCodeBlockAnalysisResult>() {

				@Override
				public int compare(HitCodeBlockAnalysisResult o1, HitCodeBlockAnalysisResult o2) {
					return (int) (o2.ochiai * 100 - o1.ochiai * 100);
				}
			});
			System.out.println("Codeblock, Ochiai, Jaccard, Russel and Rao, binary");
			for (HitCodeBlockAnalysisResult cbResult : hitList) {
				System.out.println(cbResult.codeBlock.toString() + ", " + cbResult.ochiai + ", " + cbResult.jaccard
						+ ", " + cbResult.russelAndRao + ", " + cbResult.binary);
			}
			break;
		case CountSpectrum:
			CountSpectrumSuite cSuite = new CountSpectrumSuite();
			for (int i = 1; i < args.length; i++) {
				cSuite.addSpectrum(persistor.load(args[i]));
			}
			cosine = cSuite.calcCosineSimilarity();
			jaccard = cSuite.calcJaccard();
			List<CountCodeBlockAnalysisResult> countList = new ArrayList<>(cSuite.allCodeBlocks.size());
			for (CodeBlock cb : cSuite.allCodeBlocks) {
				CountCodeBlockAnalysisResult codeBlockAnalysisResult = new CountCodeBlockAnalysisResult(cb,
						cosine.get(cb), jaccard.get(cb));
				countList.add(codeBlockAnalysisResult);
			}

			Collections.sort(countList, new Comparator<CountCodeBlockAnalysisResult>() {

				@Override
				public int compare(CountCodeBlockAnalysisResult o1, CountCodeBlockAnalysisResult o2) {
					return (int) (o2.cosine * 100 - o1.cosine * 100);
				}
			});
			System.out.println("Codeblock, Cosine, Jaccard");
			for (CountCodeBlockAnalysisResult cbResult : countList) {
				System.out.println(cbResult.codeBlock.toString() + ", " + cbResult.cosine + ", " + cbResult.jaccard);

			}
			break;
		}
	}
}
