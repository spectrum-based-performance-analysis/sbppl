package sbppl.spectrum;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Describes the API to interact with Spectrums of all types
 * 
 * A spectrum must be filled with the execution details by the methods inc() and
 * setCount(). It's contents may be extracted by the getCountFor() method.
 * 
 * @author Florian Barth
 *
 */
public abstract class AbstractSpectrum {

	/**
	 * Identifier for this run
	 * 
	 * Example: Name of the unit test used to create this spectrum
	 */
	public final String runIdentifier;
	/**
	 * Was a performance problem found during execution
	 */
	private boolean containsPerformanceProblem;
	/**
	 * Identifies used version of the Software
	 */
	public final String versionIdentifier;
	private Map<CodeBlock, Integer> codeBlockCounter;

	/**
	 * Initializes the fields that identify a spectrum
	 * 
	 * @param runIdentifier
	 *            Identifier for this run
	 * @param containsPerformanceProblem
	 *            Was a performance problem found during execution
	 * @param versionIdentifier
	 *            Identifies used version of the Software
	 */
	public AbstractSpectrum(String runIdentifer, String versionIdentifier) {
		this.runIdentifier = runIdentifer;
		this.versionIdentifier = versionIdentifier;
		this.containsPerformanceProblem = false;
		codeBlockCounter = new HashMap<>();

	}

	/**
	 * Increases the count of <code>block</code> by one
	 * 
	 * @param block
	 *            Identifies the block which count should be increased
	 */
	public abstract void inc(CodeBlock block);

	/**
	 * Set the counter for a given codeblock.
	 * 
	 * @param block
	 *            The block for which the counter should be set
	 * @param count
	 *            The amount to which the counter should be set
	 * @throws IllegalArgumentException
	 *             Is thrown for negative values or values which are not allowed
	 *             for the specific spectrum type
	 */
	public void setCount(CodeBlock block, int count) throws IllegalArgumentException {
		if (count < 0)
			throw new IllegalArgumentException("No negative values for Count allowed");
		codeBlockCounter.put(block, count);
	}

	/**
	 * Returns the count for a given code block
	 * 
	 * @param block
	 *            The block for which the count is requested
	 * @return The number of times this block was executed
	 */
	public int getCountFor(CodeBlock block) {
		Integer result = codeBlockCounter.get(block);
		if (result == null)
			result = 0;
		return result;
	}

	public Set<CodeBlock> getAllCodeBlocks() {
		return codeBlockCounter.keySet();
	}

	public boolean containsPerformanceProblem() {
		return containsPerformanceProblem;
	}

	public void setContainsPerformanceProblem(boolean containsPerformanceProblem) {
		this.containsPerformanceProblem = containsPerformanceProblem;
	}

}
