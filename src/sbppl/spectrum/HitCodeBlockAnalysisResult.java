package sbppl.spectrum;

public class HitCodeBlockAnalysisResult {

	public final CodeBlock codeBlock;
	public final double binary;
	public final double jaccard;
	public final double ochiai;
	public final double russelAndRao;

	public HitCodeBlockAnalysisResult(CodeBlock cb, double binary, double jaccard, double ochiai, double russelAndRao) {
		this.codeBlock = cb;
		this.binary = binary;
		this.jaccard = jaccard;
		this.ochiai = ochiai;
		this.russelAndRao = russelAndRao;
	}

}
