package sbppl.spectrum;

import static javax.xml.stream.XMLStreamConstants.CHARACTERS;
import static javax.xml.stream.XMLStreamConstants.START_ELEMENT;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

/**
 * Saves and loads AbstractSpectrums
 * 
 * @author Florian Barth
 *
 */
public class SpectrumPersistor {

	private static final String SPECTRUM = "Spectrum";
	private static final String TYPE = "type";
	private static final String RUN_IDENTIFIER = "RunIdentifier";
	private static final String CONTAINS_PERFORMANCE_PROBLEMS = "ContainsPerformanceProblems";
	private static final String VERSION_IDENTIFIER = "VersionIdentifier";
	private static final String EXECUTION_COUNTER = "ExecutionCounter";
	private static final String ENTRY = "Entry";
	private static final String CLASS = "class";
	private static final String METHOD = "method";
	private static final String SIGNATURE = "signature";
	private static final String PART_ID = "partId";
	private static final String SRC_LINE = "srcLine";
	private SpectrumType spectrumType;
	private CodeBlock currentCodeBlock;
	private String testMethodName;
	private boolean containsPerformanceProblems;
	private String versionIdentifier;

	/**
	 * Saves the AbstractSpectrum spec into an XML file at path
	 * 
	 * @param spec
	 *            Spectrum to save
	 * @param path
	 *            path to the file in which the spectrum should be saved
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	public void save(AbstractSpectrum spec, String path) throws XMLStreamException, IOException {
		XMLOutputFactory xmlFactory = XMLOutputFactory.newFactory();
		XMLStreamWriter writer = xmlFactory.createXMLStreamWriter(new FileWriter(path));
		writer.writeStartDocument();
		writer.writeStartElement(SPECTRUM);
		writer.writeAttribute(TYPE, spec.getClass().getSimpleName());
		writeTag(writer, RUN_IDENTIFIER, spec.runIdentifier);
		writeTag(writer, CONTAINS_PERFORMANCE_PROBLEMS, new Boolean(spec.containsPerformanceProblem()).toString());
		writeTag(writer, VERSION_IDENTIFIER, spec.versionIdentifier);
		writer.writeStartElement(EXECUTION_COUNTER);
		for (CodeBlock codeBlockEntry : spec.getAllCodeBlocks()) {
			writer.writeStartElement(ENTRY);
			writer.writeAttribute(CLASS, codeBlockEntry.className);
			writer.writeAttribute(METHOD, codeBlockEntry.methodName);
			writer.writeAttribute(SIGNATURE, codeBlockEntry.methodSignature);
			writer.writeAttribute(PART_ID, new Integer(codeBlockEntry.partId).toString());
			writer.writeAttribute(SRC_LINE, new Integer(codeBlockEntry.srcLine).toString());
			writer.writeCharacters(new Integer(spec.getCountFor(codeBlockEntry)).toString());
			writer.writeEndElement();
		}
		writer.writeEndDocument();
		writer.flush();
		writer.close();

	}

	/**
	 * Loads a spectrum out of the XML file at path.
	 * 
	 * @param path
	 *            Path to the XML which contains the spectrum
	 * @return the spectrum contained in <code>path</code>
	 * @throws FileNotFoundException
	 * @throws XMLStreamException
	 */
	public AbstractSpectrum load(String path) throws FileNotFoundException, XMLStreamException {
		String currentElementName = "";
		AbstractSpectrum result = null;

		XMLInputFactory xmlFactory = XMLInputFactory.newFactory();
		XMLStreamReader reader = xmlFactory.createXMLStreamReader(new FileReader(path));

		while (reader.hasNext()) {
			switch (reader.next()) {

			case START_ELEMENT:
				currentElementName = reader.getLocalName();
				extractAttributes(currentElementName, reader);
				if (currentElementName == EXECUTION_COUNTER)
					result = instantiateSpectrum(spectrumType, testMethodName, containsPerformanceProblems,
							versionIdentifier);

				break;
			case CHARACTERS:
				extractCharacters(currentElementName, result, reader);
				break;
			}
		}
		spectrumType = null;
		currentCodeBlock = null;
		testMethodName = "";
		containsPerformanceProblems = false;
		versionIdentifier = "";
		return result;

	}

	private void writeTag(XMLStreamWriter writer, String elementName, String content) throws XMLStreamException {
		writer.writeStartElement(elementName);
		writer.writeCharacters(content);
		writer.writeEndElement();
	}

	private void extractCharacters(String currentElementName, AbstractSpectrum result, XMLStreamReader reader)
			throws XMLStreamException {
		switch (currentElementName) {
		case RUN_IDENTIFIER:
			testMethodName = reader.getText();
			break;
		case CONTAINS_PERFORMANCE_PROBLEMS:
			containsPerformanceProblems = new Boolean(reader.getText());
			break;
		case VERSION_IDENTIFIER:
			versionIdentifier = reader.getText();
			break;
		case ENTRY:
			result.setCount(currentCodeBlock, Integer.parseInt(reader.getText()));
			break;
		default:
			throw new XMLStreamException("Element " + currentElementName + " should not contain characters!");
		}
	}

	private void extractAttributes(String currentElementName, XMLStreamReader reader) {
		switch (currentElementName) {
		case SPECTRUM:
			spectrumType = SpectrumType.valueOf(reader.getAttributeValue(null, TYPE));
			break;
		case ENTRY:
			currentCodeBlock = new CodeBlock(reader.getAttributeValue(null, CLASS),
					reader.getAttributeValue(null, METHOD), reader.getAttributeValue(null, SIGNATURE),
					new Integer(reader.getAttributeValue(null, PART_ID)),
					new Integer(reader.getAttributeValue(null, SRC_LINE)));
			break;
		}
	}

	private AbstractSpectrum instantiateSpectrum(SpectrumType type, String runIdentifier,
			boolean containsPerformanceProblem, String versionIdentifier) {
		AbstractSpectrum result = null;
		switch (type) {
		case CountSpectrum:
			result = new CountSpectrum(runIdentifier, versionIdentifier);
			break;
		case HitSpectrum:
			result = new HitSpectrum(runIdentifier, versionIdentifier);
		}
		result.setContainsPerformanceProblem(containsPerformanceProblem);
		return result;
	}
}
