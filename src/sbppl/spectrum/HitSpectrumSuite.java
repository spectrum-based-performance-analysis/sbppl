package sbppl.spectrum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A spectrum suite containing hit spectra only
 * 
 * USAGE: Step 1: Fill spectrum suite with either a list of spectra (by
 * constructor) or by adding single spectra one by one (by addSpectrum()) Step
 * 2: Call the various calc...() methods to get a Map of all the calculated
 * values for a specific similarity coefficient (e.g. Ochiai)
 * 
 * EXAMPLE 1: List<AbstractSpectrum> spectrumList = new
 * ArrayList<AbstractSpectrum>(); spectrumList.add(hitSpectrum1);
 * spectrumList.add(hitSpectrum2); spectrumList.add(hitSpectrum3);
 * HitSpectrumSuite specSuite = new HitSpectrumSuite(spectrumList);
 * Map<CodeBlock, Double> ochiai = specSuite.calcOchiai();
 * 
 * EXAMPLE 2: HitSpectrumSuite specSuite = new HitSpectrumSuite();
 * specSuite.addSpectrum(hitSpectrum1); specSuite.addSpectrum(hitSpectrum2);
 * specSuite.addSpectrum(hitSpectrum3); Map<CodeBlock, Double> ochiai =
 * specSuite.calcOchiai();
 * 
 * NOTE: Spectra lists with 0 elements and spectra lists containing spectra that
 * correspond to different CodeBlocks cause errors and should be avoided
 * 
 * @author Marc Schubert
 *
 */
public class HitSpectrumSuite extends AbstractSpectrumSuite {

	/**
	 * Values used as an intermediate step before calculating risk values
	 * 
	 * All the calc...() need these values. The two bits after the "a" mean: Bit
	 * 1: Was corresponding CodeBlock executed? 1=yes, 0=no Bit 2: Does the
	 * corresponding spectrum contain a performance problem? 1=yes, 0=no
	 * 
	 * a11 = number of failed runs in which the CodeBlock was executed a01 =
	 * number of failed runs in which the CodeBlock was NOT executed a10 =
	 * number of passed runs in which the CodeBlock was executed a00 = number of
	 * passed runs in which the CodeBlock was NOT executed
	 */
	private ExecutionCounter a00, a01, a10, a11;

	/**
	 * Store all the risk values for the various similarity coefficients
	 * 
	 * Every CodeBlock has a specific risk value for each similarity coefficient
	 */
	private SimilarityCoefficientResult ochiai, jaccard, russelAndRao, binary;

	/**
	 * If the preparation values a11,a01,a10,a00 have been successfully
	 * calculated
	 */
	private boolean arePrepValuesCalculated;

	/**
	 * Initiates spectrum suite with empty spectrum list
	 * 
	 * After calling this constructor the spectrum list has to be filled with
	 * spectra by calling addSpectrum()
	 */
	public HitSpectrumSuite() {
		this(new ArrayList<AbstractSpectrum>());
	}

	/**
	 * Initiates spectrum suite with pre-filled spectrum list
	 * 
	 * @param spectrumList
	 *            List of spectra, should already contain several spectra
	 */
	public HitSpectrumSuite(List<AbstractSpectrum> spectrumList) {
		super(spectrumList);

		arePrepValuesCalculated = false;

		a00 = new ExecutionCounter();
		a01 = new ExecutionCounter();
		a10 = new ExecutionCounter();
		a11 = new ExecutionCounter();

		ochiai = new SimilarityCoefficientResult();
		jaccard = new SimilarityCoefficientResult();
		russelAndRao = new SimilarityCoefficientResult();
		binary = new SimilarityCoefficientResult();
	}

	/**
	 * Calculates the preparation values a00, a01, a10, a11
	 * 
	 * @return if the calculation was successful
	 */
	private boolean calcPreparationValues() {

		for (CodeBlock cb : allCodeBlocks) {
			a00.put(cb, 0);
			a01.put(cb, 0);
			a10.put(cb, 0);
			a11.put(cb, 0);
		}
		for (CodeBlock codeBlock : allCodeBlocks) {
			for (AbstractSpectrum spectrum : spectrumList) {
				if (spectrum.containsPerformanceProblem())
					if (spectrum.getCountFor(codeBlock) == 1)
						increase(a11, codeBlock);
					else
						increase(a01, codeBlock);

				else if (spectrum.getCountFor(codeBlock) == 1)
					increase(a10, codeBlock);
				else
					increase(a00, codeBlock);
			}
		}
		arePrepValuesCalculated = true;
		return true;
	}

	private void increase(ExecutionCounter counter, CodeBlock codeBlock) {
		counter.put(codeBlock, counter.get(codeBlock) + 1);
	}

	/**
	 * Calculates and returns all the Ochiai risk values for the current
	 * spectrum list
	 * 
	 * @return a Map of the calculated risk values for this similarity
	 *         coefficient
	 */
	public SimilarityCoefficientResult calcOchiai() {
		if (!arePrepValuesCalculated) {
			if (!calcPreparationValues()) {
				return ochiai;
			}
		}
		for (CodeBlock cb : allCodeBlocks) {
			Integer a11_value = a11.get(cb);
			Integer a01_value = a01.get(cb);
			Integer a10_value = a10.get(cb);
			double result = a11_value / (Math.sqrt((a11_value + a01_value) * (a11_value + a10_value)));
			ochiai.put(cb, result);
		}
		return ochiai;
	}

	/**
	 * Calculates and returns all the Jaccard risk values for the current
	 * spectrum list
	 * 
	 * @return a Map of the calculated risk values for this similarity
	 *         coefficient
	 */
	public SimilarityCoefficientResult calcJaccard() {
		if (!arePrepValuesCalculated) {
			if (!calcPreparationValues()) {
				return jaccard;
			}
		}
		for (CodeBlock cb : allCodeBlocks) {
			jaccard.put(cb, a11.get(cb).doubleValue() / (a11.get(cb) + a01.get(cb) + a10.get(cb)));
		}
		return jaccard;
	}

	/**
	 * Calculates and returns all the Russel&Rao risk values for the current
	 * spectrum list
	 * 
	 * @return a Map of the calculated risk values for this similarity
	 *         coefficient
	 */
	public SimilarityCoefficientResult calcRusselAndRao() {
		if (!arePrepValuesCalculated) {
			if (!calcPreparationValues()) {
				return russelAndRao;
			}
		}
		for (CodeBlock cb : allCodeBlocks) {
			russelAndRao.put(cb, a11.get(cb).doubleValue() / (a11.get(cb) + a01.get(cb) + a10.get(cb) + a00.get(cb)));
		}
		return russelAndRao;
	}

	/**
	 * Calculates and returns all the Binary risk values for the current
	 * spectrum list
	 * 
	 * @return a Map of the calculated risk values for this similarity
	 *         coefficient
	 */
	public SimilarityCoefficientResult calcBinary() {
		if (!arePrepValuesCalculated) {
			if (!calcPreparationValues()) {
				return binary;
			}
		}
		for (CodeBlock cb : allCodeBlocks) {
			if (a01.get(cb) == 0) {
				binary.put(cb, 1.);
			} else {
				binary.put(cb, 0.);
			}
		}
		return binary;
	}

}

class ExecutionCounter extends HashMap<CodeBlock, Integer> {

	private static final long serialVersionUID = 1L;

}