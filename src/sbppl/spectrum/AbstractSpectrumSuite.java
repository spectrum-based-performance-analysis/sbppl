package sbppl.spectrum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * API for calculating risk values (similarity coefficients) of a set of
 * associated spectra
 * 
 * USAGE: Step 1: Either some spectra can be prepared, put into a List and be
 * the argument for the constructor OR an empty spectrum suite can be created
 * and then filled with spectra with the addSpectrum() method. Step 2: The
 * calc...() methods of the derived classes can be called to calculate risk
 * values.
 * 
 * NOTE: There should be at least 2 spectra in a spectrum suite to be able to
 * calculate any useful risk values.
 * 
 * @author Marc Schubert
 *
 */
public abstract class AbstractSpectrumSuite {

	/**
	 * List of spectra that are to be compared
	 * 
	 * Note: List should have at least 2 elements
	 */
	protected List<AbstractSpectrum> spectrumList;
	/**
	 * Set of all Code Blocks
	 */
	protected Set<CodeBlock> allCodeBlocks;

	/**
	 * Initializes a spectrum suite with all the spectra going into calculation
	 * 
	 * @param spectrumList
	 *            List of spectra going into calculation
	 */
	public AbstractSpectrumSuite(List<AbstractSpectrum> spectrumList) {
		this.spectrumList = new ArrayList<>(spectrumList);
		populateAllCodeblocks();
	}

	/**
	 * Adds a spectrum to the existing spectrum list in the spectrum suite
	 * 
	 * @param spectrum
	 *            spectrum to add to the existing spectrum list
	 */
	public void addSpectrum(AbstractSpectrum spectrum) {
		spectrumList.add(spectrum);
		allCodeBlocks.addAll(spectrum.getAllCodeBlocks());
	}

	private void populateAllCodeblocks() {
		allCodeBlocks = new HashSet<>();
		for (AbstractSpectrum spectrum : spectrumList) {
			allCodeBlocks.addAll(spectrum.getAllCodeBlocks());
		}
	}

}

class SimilarityCoefficientResult extends HashMap<CodeBlock, Double> {

	private static final long serialVersionUID = 1L;

}
