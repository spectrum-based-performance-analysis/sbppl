package sbppl.spectrum;

public class CountCodeBlockAnalysisResult {

	public final CodeBlock codeBlock;
	public final double cosine;
	public final double jaccard;

	public CountCodeBlockAnalysisResult(CodeBlock cb, double cosine, double jaccard) {
		this.codeBlock = cb;
		this.cosine = cosine;
		this.jaccard = jaccard;
	}

}
