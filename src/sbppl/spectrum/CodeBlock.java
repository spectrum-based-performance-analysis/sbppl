package sbppl.spectrum;

/**
 * Data class used to represent CodeBlocks which can be executed
 * 
 * @author Florian Barth
 *
 */
public class CodeBlock {

	/**
	 * Full qualified class name in which the executed codeblock resides
	 */
	public final String className;
	/**
	 * Name of the method in which the codeblock was executed
	 */
	public final String methodName;
	/**
	 * String that contains the method signature. So overloaded methods can be
	 * distinguished
	 */
	public final String methodSignature;
	/**
	 * Numerical ID which can be used to distinguish statements or branches in a
	 * method
	 */
	public final int partId;
	/**
	 * Souce code line where the block starts
	 */
	public final int srcLine;

	/**
	 * Constructs a CodeBlock which represents the whole method.
	 * 
	 * @param className
	 *            Full qualified class name in which the executed codeblock
	 *            resides
	 * @param methodName
	 *            Name of the method in which the codeblock was executed
	 * @param methodSignature
	 *            Signature of the method
	 */
	public CodeBlock(String className, String methodName, String methodSignature, int srcLine) {
		this(className, methodName, methodSignature, 0, srcLine);
	}

	/**
	 * Constructs a CodeBlock which is part of a method.
	 * 
	 * @param className
	 *            Full qualified class name in which the executed codeblock
	 *            resides
	 * @param methodName
	 *            Name of the method in which the codeblock was executed
	 * @param methodSignature
	 *            Signature of the method
	 * @param partId
	 *            ID of the part of the method which sould be represented
	 */
	public CodeBlock(String className, String methodName, String methodSignature, int partId, int srcLine) {
		this.className = className;
		this.methodName = methodName;
		this.methodSignature = methodSignature;
		this.partId = partId;
		this.srcLine = srcLine;

	}

	@Override
	public int hashCode() {
		return className.hashCode() + methodName.hashCode() + partId;
	}

	@Override
	public boolean equals(Object obj) {
		CodeBlock other = (CodeBlock) obj;
		if (other == null)
			return false;
		return className.equals(other.className) && methodName.equals(other.methodName) && partId == other.partId;
	}

	@Override
	public String toString() {
		return className + "." + methodName + methodSignature + " id" + partId + " @" + srcLine;
	}

}
