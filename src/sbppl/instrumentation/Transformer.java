package sbppl.instrumentation;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.security.ProtectionDomain;
import java.util.Date;
import java.util.Properties;

import javax.xml.stream.XMLStreamException;

import javassist.ClassPool;
import javassist.CtClass;
import sbppl.spectrum.SpectrumType;

public class Transformer implements ClassFileTransformer {

	private static final String SAVE_PATH = "savePath";
	private static final String EXCLUDES = "excludes";
	private static final String INCLUDES = "includes";
	private static final String VERSION = "version";
	private static final String RUN_IDENTIFIER = "runIdentifier";
	private static final String SPECTRUM_TYPE = "spectrumType";
	private Instrumentor instrumentor;
	private ClassPool classPool;
	private String[] includes;
	private String[] excludes;
	private static SpectrumType stype;
	private static String runIdentifier;
	private static String version;
	private static String includesString;
	private static String excludesString;
	private static String path;

	public Transformer(Instrumentor in, String includes, String excludes) {
		instrumentor = in;
		this.includes = includes.split(";");
		this.excludes = excludes.split(";");
		classPool = ClassPool.getDefault();
	}

	public static void premain(String agentArguments, Instrumentation instrumentation) throws IOException {
		Properties prop = new Properties(defaultProperties());
		try {
			prop.load(new FileInputStream("sbppl.conf"));
		} catch (IOException e) {
			System.out.println("Couldn't load sbppl.conf.");
		}
		readPropertiesFromFile(prop);
		configureInstrumentor(instrumentation);
		prepareSavingOfResults(prop);
	}

	private static void prepareSavingOfResults(Properties prop) throws IOException {
		String fileIdentifier = runIdentifier + "_" + version + "_" + System.currentTimeMillis();
		prop.store(new FileWriter(path + "/" + fileIdentifier + "_sbppl.conf"), "");
		final String spectraPath = path + "/" + fileIdentifier + ".xml";
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				try {
					new sbppl.spectrum.SpectrumPersistor().save(sbppl.instrumentation.Instrumentor.spec, spectraPath);
				} catch (XMLStreamException | IOException e) {
					e.printStackTrace();
				}
			}
		});
	}

	private static void configureInstrumentor(Instrumentation instrumentation) {
		Instrumentor in = new Instrumentor(stype, runIdentifier, version);
		Transformer transformer = new Transformer(in, includesString, excludesString);
		instrumentation.addTransformer(transformer);
	}

	private static void readPropertiesFromFile(Properties prop) {
		stype = SpectrumType.valueOf(prop.getProperty(SPECTRUM_TYPE));
		runIdentifier = prop.getProperty(RUN_IDENTIFIER);
		version = prop.getProperty(VERSION);
		includesString = prop.getProperty(INCLUDES);
		excludesString = prop.getProperty(EXCLUDES);
		path = prop.getProperty(SAVE_PATH);
		System.out.println("Spectrum Data: " + stype + ", " + runIdentifier + ", " + version);
		System.out.println("Classes that start with one of " + includesString + " are instrumented.");
		System.out.println("Classes that start with one of " + excludesString + " are excluded from instrumentation.");
	}

	private static Properties defaultProperties() {
		Properties def = new Properties();
		def.setProperty(SPECTRUM_TYPE, SpectrumType.CountSpectrum.toString());
		def.setProperty(RUN_IDENTIFIER, new Date().toString());
		def.setProperty(VERSION, "unknown version");
		def.setProperty(INCLUDES, "");
		def.setProperty(EXCLUDES, "sun;java;sbppl.instrumentation;sbppl.spectrum");
		def.setProperty(SAVE_PATH, "results");
		return def;
	}

	@Override
	public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
			ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
		String classNameWithDots = className.replace("/", ".");
		for (String exclude : excludes)
			if (classNameWithDots.startsWith(exclude))
				return classfileBuffer;
		for (String include : includes)
			if (classNameWithDots.startsWith(include)) {
				try {
					System.out.println("Instrumenting " + classNameWithDots);
					CtClass classToTransform = classPool.get(classNameWithDots);
					instrumentor.instrument(classToTransform);
					return classToTransform.toBytecode();
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("Returning untouched classFile for: " + classNameWithDots);
					return classfileBuffer;
				}
			}
		return classfileBuffer;
	}

}
