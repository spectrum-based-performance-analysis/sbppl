package sbppl.instrumentation;

import javassist.CannotCompileException;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.bytecode.BadBytecode;
import javassist.bytecode.analysis.ControlFlow;
import javassist.bytecode.analysis.ControlFlow.Block;
import sbppl.spectrum.AbstractSpectrum;
import sbppl.spectrum.CodeBlock;
import sbppl.spectrum.CountSpectrum;
import sbppl.spectrum.HitSpectrum;
import sbppl.spectrum.SpectrumType;

public class Instrumentor {

	public static AbstractSpectrum spec;
	private String className;
	private String methodName;
	private String signature;
	private int methodStartingLine;

	/**
	 * Creates an Instrumentor which uses a Spectrum with the given parameter
	 * 
	 * @param spectrumType
	 * @param runIdentifier
	 * @param version
	 */
	public Instrumentor(SpectrumType spectrumType, String runIdentifier, String version) {
		switch (spectrumType) {
		case HitSpectrum:
			spec = new HitSpectrum(runIdentifier, version);
			break;
		case CountSpectrum:
			spec = new CountSpectrum(runIdentifier, version);
			break;
		}
	}

	/**
	 * Instruments every method declared in classToInstrument
	 * 
	 * At the beginning of each block/branch a call to <code>spec.inc()</code>
	 * is added with a CodeBlock object representing the current branch
	 * 
	 * @param classToInstrument
	 *            Class which methods will be instrumented
	 * @throws CannotCompileException
	 * @throws BadBytecode
	 */
	public void instrument(CtClass classToInstrument) throws CannotCompileException {
		for (CtMethod method : classToInstrument.getDeclaredMethods()) {
			CtMethod backup = null;
			try {
				if (method.isEmpty())
					continue;
				backup = new CtMethod(method, classToInstrument, null);
				instrumentMethod(classToInstrument, method);
			} catch (CannotCompileException | BadBytecode e) {
				System.out.println("Exception instrumenting method: " + method.getName());
				e.printStackTrace();
				method.setBody(backup, null);
			}
		}

	}

	private void instrumentMethod(CtClass classToInstrument, CtMethod method)
			throws CannotCompileException, BadBytecode {
		className = classToInstrument.getName();
		methodName = method.getName();
		signature = method.getSignature();
		methodStartingLine = method.getMethodInfo2().getLineNumber(0);
		String insertedSrc = "sbppl.instrumentation.Instrumentor.spec.inc(new sbppl.spectrum.CodeBlock(\"" + className
				+ "\",\"" + methodName + "\",\"" + signature + "\", " + methodStartingLine + "));";
		method.insertBefore(insertedSrc);
		instrumentBlocks(method);
	}

	private void instrumentBlocks(CtMethod method) throws BadBytecode, CannotCompileException {
		ControlFlow cf = new ControlFlow(method);
		int partId = 1;
		int lastPosition = 0;
		int lastInserted = 0;
		int firstBlockPosition = 0;
		int offset = 0;
		int len = cf.basicBlocks().length;
		for (int i = 0; i < len; i++) {
			Block block = cf.basicBlocks()[i];
			int position = block.position();
			if (firstBlockPosition == 0) {
				firstBlockPosition = position;
				offset = methodStartingLine - firstBlockPosition;
			}
			position += offset;
			if (!positionDidChange(lastPosition, position)) {
				position++;
			}
			lastPosition = position;
			int insertedPos = method.insertAt(position, false, "//no op");
			if (positionDidChange(lastInserted, insertedPos)) {
				String insertedSrc = "sbppl.instrumentation.Instrumentor.spec.inc(new sbppl.spectrum.CodeBlock(\""
						+ className + "\",\"" + methodName + "\",\"" + signature + "\", " + partId + ", " + insertedPos
						+ "));";
				insertedPos = method.insertAt(position, true, insertedSrc);
			} else {
				continue;
			}
			lastInserted = insertedPos;
			partId++;

		}
	}

	private boolean positionDidChange(int lastPosition, int position) {
		return position != lastPosition;
	}

	public CodeBlock codeBlockByMethod(CtMethod method) {
		String className = method.getDeclaringClass().getName();
		String methodName = method.getName();
		String signature = method.getSignature();
		int srcLine = method.getMethodInfo2().getLineNumber(0);
		return new CodeBlock(className, methodName, signature, srcLine);
	}

}
