#!/bin/zsh
mkdir -p bin/META_INF
javac src/**/*.java -d bin -classpath lib/hamcrest-core-1.3.jar:lib/javassist-3.20.0-GA.jar:~/kieker-1.12/build/libs/kieker-1.12-aspectj.jar

cp ~/ownCloud/Flo/Uni\ Stuttgart/Semester\ 1\ \(SS16\)/Hauptseminar\ 1/generated_applications/small_test/src-resources/META_INF/* bin/META_INF
